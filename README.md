# jf/serializer

Serializadores de objetos y otros tipos de datos.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/serializer` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/serializer
```

#### Dependencias

Cuando el proyecto es instalado, adicionalmente se instalan las siguientes dependencias:

| Paquete | Versión |
|:--------|:--------|
| jf/base | ^4.0    |

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone https://www.gitlab.com/jfphp/jfSerializer.git
cd jfSerializer
composer install
```

## Archivos disponibles

### Clases

| Nombre                                                                       | Descripción                                                                                                                                                     |
|:-----------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [jf\Serializer\Flatten](src/Flatten.php)                                     | Serializa de manera recursiva todos los valores de los objetos y arrays aplanando el resultado a un solo nivel.                                                 |
| [jf\Serializer\Json](src/Json.php)                                           | Serializador que delega en la interfaz `JsonSerializable` para la serialización de los objetos.                                                                 |
| [jf\Serializer\Properties](src/Properties.php)                               | Serializador de las propiedades públicas de un objeto iterando de manera recursiva sobre cualquier objeto o array que se encuentre como valor de una propiedad. |
| [jf\Serializer\Scalar](src/Scalar.php)                                       | Serializa un objeto dejando solamente los valores escalares o los listados de escalares.                                                                        |
| [jf\Serializer\Serializer](src/Serializer.php)                               | Serializador simple para los tipos de datos básicos de PHP y algunos objetos conocidos.                                                                         |
| [jf\Serializer\Sorter\ASorter](src/Sorter/ASorter.php)                       | Clase base para los ordenadores de arrays y/o listas.                                                                                                           |
| [jf\Serializer\Sorter\KeysAlphabetically](src/Sorter/KeysAlphabetically.php) | Ordena alfabéticamente las claves de un array de manera recursiva o no.                                                                                         |
| [jf\Serializer\Sorter\ScalarFirst](src/Sorter/ScalarFirst.php)               | Ordena de manera recursiva un array aplicando el siguiente criterio:                                                                                            |

### Interfaces

| Nombre                                                 | Descripción                                                             |
|:-------------------------------------------------------|:------------------------------------------------------------------------|
| [jf\Serializer\IGetSerializer](src/IGetSerializer.php) | Interfaz para detectar aquellas clases que disponen de un serializador. |
| [jf\Serializer\ISerializer](src/ISerializer.php)       | Interfaz para la serialización de valores.                              |
| [jf\Serializer\Sorter\ISorter](src/Sorter/ISorter.php) | Interfaz para ordenar los arrays y/o listas.                            |

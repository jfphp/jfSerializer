<?php

namespace jf\Serializer;

/**
 * Serializa de manera recursiva todos los valores de los
 * objetos y arrays aplanando el resultado a un solo nivel.
 */
class Flatten extends Serializer
{
    /**
     * Aplana un array dejando solamente un nivel de anidamiento.
     *
     * No se ejecuta de manera recursiva ya que se serializeArray lo llama para
     * cada array que genera, empezando por los más internos.
     *
     * @param array  $value Listado a aplanar
     * @param string $glue  Texto a usar para unir los identificadores.
     *
     * @return void
     */
    public static function flatten(array &$value, string $glue = '.') : void
    {
        foreach ($value as $k => $v)
        {
            if (is_array($v))
            {
                foreach ($v as $k2 => $v2)
                {
                    $value["$k$glue$k2"] = $v2;
                }
                unset($value[ $k ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function serializeArray(array $value, string $id = '') : array
    {
        $value = parent::serializeArray($value, $id);
        static::flatten($value);

        return $value;
    }
}
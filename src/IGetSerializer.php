<?php

namespace jf\Serializer;

/**
 * Interfaz para detectar aquellas clases que disponen de un serializador.
 */
interface IGetSerializer
{
    /**
     * Devuelve el serializador usado por la clase.
     *
     * @return ISerializer
     */
    public function getSerializer() : ISerializer;
}
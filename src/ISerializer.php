<?php

namespace jf\Serializer;

use DateTimeInterface;
use UnitEnum;

/**
 * Interfaz para la serialización de valores.
 *
 * Las funciones aceptan un identificador del valor que se desea serializar
 * por si se desea cambiar el formato en función dicho valor.
 *
 * Por ejemplo, se puede decidir formatear un valor llamado `endDate` de manera diferente
 * al valor `startDate` aunque ambos sean de tipo fecha.
 */
interface ISerializer
{
    /**
     * Serializa un valor de cualquier tipo llamando al método `serialize*` apropiado.
     *
     * @param mixed  $value Valor a serializar.
     * @param string $id    Identificador del valor que se va a serializar.
     *
     * @return mixed
     */
    public function serialize(mixed $value, string $id = '') : mixed;

    /**
     * Serializa un array.
     *
     * @param mixed  $value Valor a serializar.
     * @param string $id    Identificador del valor que se va a serializar.
     *
     * @return mixed
     */
    public function serializeArray(array $value, string $id = '') : array;

    /**
     * Serializa un valor booleano.
     *
     * @param mixed  $value Valor a serializar.
     * @param string $id    Identificador del valor que se va a serializar.
     *
     * @return mixed
     */
    public function serializeBoolean(bool $value, string $id = '') : bool;

    /**
     * Serializa una fecha.
     *
     * @param DateTimeInterface $value Valor a serializar.
     * @param string            $id    Identificador del valor que se va a serializar.
     *
     * @return int|string
     */
    public function serializeDate(DateTimeInterface $value, string $id = '') : int|string;

    /**
     * Serializa una fecha.
     *
     * @param UnitEnum $value Valor a serializar.
     * @param string   $id    Identificador del valor que se va a serializar.
     *
     * @return int|string
     */
    public function serializeEnum(UnitEnum $value, string $id = '') : int|string;

    /**
     * Serializa un valor de coma flotante.
     *
     * @param float  $value Valor a serializar.
     * @param string $id    Identificador del valor que se va a serializar.
     *
     * @return mixed
     */
    public function serializeFloat(float $value, string $id = '') : float;

    /**
     * Serializa un valor entero.
     *
     * @param int    $value Valor a serializar.
     * @param string $id    Identificador del valor que se va a serializar.
     *
     * @return int
     */
    public function serializeInteger(int $value, string $id = '') : int;

    /**
     * Serializa un valor nulo.
     *
     * @param string $id Identificador del valor que se va a serializar.
     *
     * @return mixed
     */
    public function serializeNull(string $id = '') : mixed;

    /**
     * Serializa un objeto.
     *
     * @param object $value Objeto a serializar.
     * @param string $id    Identificador del valor que se va a serializar.
     *
     * @return mixed
     */
    public function serializeObject(object $value, string $id = '') : mixed;

    /**
     * Serializa un recurso.
     *
     * @param resource $value Valor a serializar.
     * @param string   $id    Identificador del valor que se va a serializar.
     *
     * @return mixed
     */
    public function serializeResource(mixed $value, string $id = '') : mixed;

    /**
     * Serializa un texto.
     *
     * @param string $value Valor a serializar.
     * @param string $id    Identificador del valor que se va a serializar.
     *
     * @return mixed
     */
    public function serializeString(string $value, string $id = '') : string;
}
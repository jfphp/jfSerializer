<?php

namespace jf\Serializer;

use JsonSerializable;

/**
 * Serializador que delega en la interfaz `JsonSerializable` para la serialización de los objetos.
 */
class Json extends Serializer
{
    /**
     * @inheritdoc
     */
    public function serializeObject(object $value, string $id = '') : mixed
    {
        if ($value === $this)
        {
            $value = NULL;
        }
        elseif ($value instanceof JsonSerializable)
        {
            $value = $value->jsonSerialize();
        }
        else
        {
            $value = parent::serializeObject($value);
        }

        return $value && is_array($value)
            ? $this->serializeArray($value)
            : $value;
    }
}
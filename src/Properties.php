<?php

namespace jf\Serializer;

use DateTimeInterface;
use jf\Collection\ACollection;
use UnitEnum;

/**
 * Serializador de las propiedades públicas de un objeto iterando de manera recursiva
 * sobre cualquier objeto o array que se encuentre como valor de una propiedad.
 */
class Properties extends Serializer
{
    /**
     * @inheritdoc
     */
    public function serializeObject(object $value, string $id = '') : mixed
    {
        return $value instanceof DateTimeInterface ||
               $value instanceof UnitEnum ||
               (class_exists(ACollection::class) && $value instanceof ACollection)
            ? parent::serializeObject($value, $id)
            : $this->serializeArray((array) $value, $id);
    }
}
<?php

namespace jf\Serializer;

/**
 * Serializa un objeto dejando solamente los valores escalares o los listados de escalares.
 */
class Scalar extends Properties
{
    /**
     * Indica si el valor es una lista de escalares.
     *
     * A diferencia de la función `array_is_list` los índices no
     * tienen que empezar de cero ni ser sucesivos.
     *
     * @param mixed $value Valor a verificar.
     *
     * @return bool
     */
    public static function isScalarList(mixed $value) : bool
    {
        $is = is_array($value);
        if ($is)
        {
            foreach ($value as $k => $v)
            {
                if (!is_int($k) || !is_scalar($v))
                {
                    $is = FALSE;
                    break;
                }
            }
        }

        return $is;
    }

    /**
     * @inheritdoc
     */
    public function isValid(mixed $value) : bool
    {
        return parent::isValid($value) && (is_array($value) ? static::isScalarList($value) : is_scalar($value));
    }
}
<?php

namespace jf\Serializer;

use BackedEnum;
use Closure;
use DateTimeInterface;
use jf\Base\ABase;
use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Base\Serializable\TAssign;
use jf\Collection\ACollection;
use jf\Serializer\Sorter\ISorter;
use jf\Serializer\Sorter\KeysAlphabetically;
use JsonSerializable;
use UnitEnum;

/**
 * Serializador simple para los tipos de datos básicos de PHP y algunos objetos conocidos.
 */
class Serializer extends ABase implements IAssign, ISerializer
{
    use TAssign;

    /**
     * Formato para serializar la fecha.
     *
     * @var Closure|string
     */
    public Closure|string $dateFormat = 'c';

    /**
     * Indica si se serializan los arrays vacíos.
     *
     * @var bool
     */
    public bool $emptyArray = FALSE;

    /**
     * Indica si se serializan las cadenas vacías.
     *
     * @var bool
     */
    public bool $emptyString = FALSE;

    /**
     * Indica si se serializan los valores nulos.
     *
     * @var bool
     */
    public bool $nullable = FALSE;

    /**
     * Instancia a usar para ordenar el resultado.
     *
     * @var ISorter|NULL
     */
    public ISorter|null $sorter = NULL;

    public function __construct(array $values = [])
    {
        if (!array_key_exists('sorter', $values))
        {
            $values['sorter'] = new KeysAlphabetically();
        }
        $this->assign($values);
    }

    /**
     * Devuelve la clave a usar la serialización para elementos anidados
     * en función del identificador del valor actual y el del padre.
     *
     * @param string $parent Identificador del valor del padre.
     * @param string $id     Identificador del elemento actual.
     *
     * @return string
     *
     * @noinspection PhpUnusedParameterInspection
     */
    protected function getKey(string $parent, string $id) : string
    {
        return $id && str_contains($id, "\0")
            // Si se serializan propiedades protegidas y privadas usando PHP directamente
            // sus nombres tienen un valor nulo que se debe eliminar.
            ? substr($id, strrpos($id, "\0") + 1)
            : $id;
    }

    /**
     * Indica si el valor es un valor vacío. A diferencia de `empty($value)` este
     * método no considera `false` ni `0` como un valor vacío.
     *
     * @param mixed $value Valor a verificar.
     *
     * @return bool
     */
    public function isEmpty(mixed $value) : bool
    {
        return $value === NULL || $value === [] || $value === '';
    }

    /**
     * Indica si el valor es válido para poder agregarlo al resultado.
     *
     * @param mixed $value Valor a verificar.
     *
     * @return bool
     */
    public function isValid(mixed $value) : bool
    {
        return !$this->isEmpty($value) ||
               ($value === NULL && $this->nullable) ||
               ($value === [] && $this->emptyArray) ||
               ($value === '' && $this->emptyString);
    }

    /**
     * @inheritdoc
     */
    public function serialize(mixed $value, string $id = '') : mixed
    {
        return match (gettype($value))
        {
            'NULL'     => $this->serializeNull($id),
            'array'    => $this->serializeArray($value, $id),
            'boolean'  => $this->serializeBoolean($value, $id),
            'double'   => $this->serializeFloat($value, $id),
            'integer'  => $this->serializeInteger($value, $id),
            'object'   => $this->serializeObject($value, $id),
            'resource' => $this->serializeResource($value, $id),
            'string'   => $this->serializeString($value, $id)
        };
    }

    /**
     * @inheritdoc
     */
    public function serializeArray(array $value, string $id = '') : array
    {
        $id2        = $id && $id[ -1 ] !== '.' ? "$id." : '';
        $serialized = [];
        foreach ($value as $aname => $avalue)
        {
            $avalue = $this->serialize($avalue, $id2 . $aname);
            if ($this->isValid($avalue))
            {
                $serialized[ $this->getKey($id, $aname) ] = $avalue;
            }
        }
        if (!array_is_list($serialized))
        {
            $this->sorter?->sort($serialized);
        }

        return $serialized;
    }

    /**
     * @inheritdoc
     */
    public function serializeBoolean(bool $value, string $id = '') : bool
    {
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function serializeDate(DateTimeInterface $value, string $id = '') : string
    {
        $format = $this->dateFormat;

        return is_callable($format)
            ? $format($value, $id)
            : $value->format($format ?: 'c');
    }

    /**
     * @inheritdoc
     */
    public function serializeEnum(UnitEnum $value, string $id = '') : int|string
    {
        return $value instanceof BackedEnum
            ? $value->value
            : $value->name;
    }

    /**
     * @inheritdoc
     */
    public function serializeFloat(float $value, string $id = '') : float
    {
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function serializeInteger(int $value, string $id = '') : int
    {
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function serializeNull(string $id = '') : null
    {
        return NULL;
    }

    /**
     * @inheritdoc
     */
    public function serializeObject(object $value, string $id = '') : mixed
    {
        if ($value === $this)
        {
            $value = NULL;
        }
        elseif ($value instanceof UnitEnum)
        {
            $value = $this->serializeEnum($value);
        }
        elseif ($value instanceof DateTimeInterface)
        {
            $value = $this->serializeDate($value, $id);
        }
        elseif (class_exists(ACollection::class) && $value instanceof ACollection)
        {
            $value = $value->values();
        }
        elseif ($value instanceof IToArray)
        {
            $value = $value->toArray();
        }
        elseif ($value instanceof JsonSerializable)
        {
            $value = $value->jsonSerialize();
        }
        else
        {
            $tmp = [];
            foreach (((array) $value) as $prop => $val)
            {
                if ($prop[0] !== '_' && !str_contains($prop, "\0"))
                {
                    $tmp[ $prop ] = $val;
                }
            }
            $value = $tmp;
        }

        return $value && is_array($value)
            ? $this->serializeArray($value, $id)
            : $value;
    }

    /**
     * @inheritdoc
     */
    public function serializeResource(mixed $value, string $id = '') : null
    {
        return NULL;
    }

    /**
     * @inheritdoc
     */
    public function serializeString(string $value, string $id = '') : string
    {
        return $value;
    }
}
<?php

namespace jf\Serializer\Sorter;

use jf\Base\ABase;
use jf\Base\Serializable\TAssign;

/**
 * Clase base para los ordenadores de arrays y/o listas.
 */
abstract class ASorter extends ABase implements ISorter
{
    use TAssign;

    /**
     * Listado de claves que no serán ordenadas.
     *
     * @var string[]
     */
    public array $ignore = [];

    /**
     * Indica si se deben ordenar las listas de escalares.
     *
     * @var bool
     */
    public bool $list = TRUE;

    /**
     * Carácter a eliminar al principio del nombre antes de hacer la comparación.
     *
     * @var string
     */
    public string $removePrefix = '_';

    /**
     * Compara las claves tomando en cuenta si es un array o no colocando
     * primero los valores escalares en orden alfabético y luego los array
     * también en orden alfabético.
     *
     * @param array  $array Array con los valores.
     * @param string $key1  Clave a comparar.
     * @param string $key2  Clave a comparar.
     *
     * @return int
     */
    public function cmpKeys(array $array, string $key1, string $key2) : int
    {
        return $this->cmpNames($key1, $key2);
    }

    /**
     * Compara los nombres de métodos en PHP, poniendo de primero los métodos
     * mágicos ordenados alfabéticamente y luego el resto de métodos también
     * en orden alfabético sin importar si empiezan por un `_`.
     *
     * @param string $name1 Nombre a comparar.
     * @param string $name2 Nombre a comparar.
     *
     * @return int
     */
    public function cmpNames(string $name1, string $name2) : int
    {
        $prefix = $this->removePrefix;
        if ($prefix)
        {
            $dprefix = $prefix . $prefix;
            if (str_starts_with($name1, $dprefix))
            {
                $result = str_starts_with($name2, $dprefix) ? strcasecmp($name1, $name2) : -1;
            }
            elseif (str_starts_with($name2, $dprefix))
            {
                $result = 1;
            }
            else
            {
                $result = strcasecmp(ltrim($name1, $prefix), ltrim($name2, $prefix));
            }
        }
        else
        {
            $result = strcasecmp($name1, $name2);
        }

        return $result;
    }

    /**
     * Ordena un array siguiente el algoritmo definido en `cmpKeys`.
     *
     * @param array $array Array a ordenar.
     *
     * @return void
     */
    public function sortKeys(array &$array) : void
    {
        uksort($array, fn($k1, $k2) => $this->cmpKeys($array, $k1, $k2));
    }

    /**
     * Ordena una lista en función de los tipos de datos que almacena.
     *
     * @param array $list  Lista a ordenar.
     * @param array $types Tipos de datos que almacena la lista.
     *
     * @return array
     */
    public function sortList(array &$list, array $types = []) : array
    {
        if ($list)
        {
            if (!$types)
            {
                foreach ($list as $value)
                {
                    $types[ gettype($value) ] = 1;
                }
            }
            if (!isset($types['array']) && !isset($types['object']) && !isset($types['resource']))
            {
                $types = array_keys($types);
                sort($types);
                match (implode('|', $types))
                {
                    'int|string' => sort($list, SORT_NUMERIC),
                    'string'     => sort($list, SORT_FLAG_CASE | SORT_STRING),
                    default      => sort($list)
                };
            }
        }

        return $list;
    }
}

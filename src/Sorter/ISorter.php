<?php

namespace jf\Serializer\Sorter;

/**
 * Interfaz para ordenar los arrays y/o listas.
 */
interface ISorter
{
    /**
     * Ordena los valores del array o lista.
     *
     * @param array $values    Valores a ordenar.
     * @param bool  $recursive Indica si se ordenan los elementos del array que también son arrays.
     *
     * @return array El array ordenado.
     */
    public function sort(array &$values, bool $recursive = TRUE) : array;
}

<?php

namespace jf\Serializer\Sorter;

/**
 * Ordena alfabéticamente las claves de un array de manera recursiva o no.
 */
class KeysAlphabetically extends ASorter
{
    /**
     * @inheritdoc
     */
    public function sort(array &$values, bool $recursive = TRUE) : array
    {
        if ($recursive)
        {
            $ignore = $this->ignore;
            foreach ($values as $key => $value)
            {
                if (is_array($value) && (!$ignore || !in_array($key, $ignore)))
                {
                    $this->sort($values[ $key ], $recursive);
                }
            }
        }
        if (array_is_list($values))
        {
            if ($this->list)
            {
                $this->sortList($values);
            }
        }
        else
        {
            $this->sortKeys($values);
        }

        return $values;
    }
}

<?php

namespace jf\Serializer\Sorter;

/**
 * Ordena de manera recursiva un array aplicando el siguiente criterio:
 *
 * - Asociativo: se ordenan por las claves, colocando primero los escalares y luego el resto.
 * - Lista de escalares, se ordenan por su valor.
 * - Lista con algún elemento no escalar, no se ordena.
 */
class ScalarFirst extends ASorter
{
    /**
     * @inheritdoc
     */
    public function cmpKeys(array $array, string $key1, string $key2) : int
    {
        return match (TRUE)
        {
            is_array($array[ $key1 ]) => is_array($array[ $key2 ]) ? $this->cmpNames($key1, $key2) : 1,
            is_array($array[ $key2 ]) => -1,
            default                   => $this->cmpNames($key1, $key2)
        };
    }

    /**
     * @inheritdoc
     */
    public function sort(array &$values, bool $recursive = TRUE) : array
    {
        $ignore = $this->ignore;
        $isList = TRUE;
        $types  = [];
        foreach ($values as $key => $value)
        {
            $types[ gettype($value) ] = 1;
            if ($recursive && is_array($value) && (!$ignore || !in_array($key, $ignore)))
            {
                static::sort($values[ $key ], $recursive);
            }
            $isList = $isList && is_numeric($key);
        }
        if ($isList)
        {
            if ($this->list)
            {
                $this->sortList($values, $types);
            }
        }
        else
        {
            $this->sortKeys($values);
        }

        return $values;
    }
}
